from datetime import datetime
from flask import Flask


app = Flask(__name__)

@app.route('/api/index')
def index():
   return "Running"
@app.route('/api/time')
def get_current_time():
    return datetime.now()

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
